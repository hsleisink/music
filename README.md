Music Theory
============

This tool explains how scales are formed based on a root note and a modality, why some chords are major and others are minor and why there is one diminished chord.
