const BLOCK_WIDTH = 40;
const SHARP = '&#x266f;';
const FLAT = '&#x266d;';

var languages = {
	en: 'English',
	nl: 'Nederlands'
};
const titles = {
	nl: 'Muziektheorie',
	en: 'Music theory'
};
const labels = {
	nl: [ 'Modaliteit', 'Noten', 'Intervallen', 'Toonladder', 'Akkoorden', 'Uitgangspunt', 'Huidig' ],
	en: [ 'Modality', 'Notes', 'Intervals', 'Scale', 'Chords', 'Basis', 'Current' ]
};
const modality = {
	nl: [ 'ionisch (majeur)', 'dorisch', 'frygisch', 'lydisch', 'mixolydisch', 'eolisch (mineur)', 'locrisch' ],
	en: [ 'ionian (major)', 'dorian', 'phrygian', 'lydian', 'mixolydian', 'aeolian (minor)', 'locrian' ]
};
const modality_differences = {
	nl: [ 'Verschillen tussen de #1 en de #2 modaliteit' ],
	en: [ 'Differences between the #1 and the #2 modality' ]
};

const intervals = [ '1', '', '', 'm3', 'M3', '', 'd5', 'P5', '', '', 'm7', 'M7' ];
const notes_sharps = [ 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B' ];
const notes_flats = [ 'C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab', 'A', 'Bb', 'B' ];
const scale_steps = [ 2, 2, 1, 2, 2, 2, 1 ];

const major_modes = [ 0, 3, 4 ];
const minor_modes = [ 5, 1, 2, 6 ];

/* Initialize page
 */
function init_page(language) {
	$('div.label').empty();
	$('div.line').empty();

	$('div.notes button').eq(2).html(SHARP);
	$('div.notes button').eq(3).html(FLAT);

	/* Language
	 */
	$('head title').text(titles[language]);
	$('body h1').text(titles[language]);

	$('div.modality div.label').text(labels[language][0] + ':');
	$('div.notes div.label').text(labels[language][1] + ':');
	$('div.intervals div.label').text(labels[language][2] + ':');
	$('div.scale div.label').text(labels[language][3] + ':');
	$('div.chords div.label').text(labels[language][4] + ':');
	$('div.basis div.label').text(labels[language][5] + ':');
	$('div.difference div.label').text(labels[language][6] + ':');

	/* Modality
	 */
	modality[language].forEach(function(modus, i) {
		var item = $('<div class="item" steps="' + scale_steps[i] + '"><div>' + modus + '</div><div>&vert;</div></div>');
		item.css('width', (scale_steps[i] * BLOCK_WIDTH) + 'px');
		$('div.modality div.line').append(item);
	});

	/* Notes
	 */
	show_notes(notes_sharps);
	$('div.modality').attr('shifted', 0);
	$('div.notes').attr('shifted', 0);
	$('div.intervals').attr('shifted', 0);

	/* Intervals
	 */
	intervals.forEach(function(interval) {
		var item = $('<div class="item">' + interval + '</div>');
		item.css('width', BLOCK_WIDTH + 'px');
		$('div.intervals div.line').append(item);
	});
}

function show_notes(notes) {
	$('div.notes div.line').empty();

	notes.forEach(function(note, index) {
		note = note.replace('#', SHARP);
		note = note.replace('b', FLAT);

		var item = $('<div class="item" index="' + index + '">' + note + '</div>');
		item.css('width', BLOCK_WIDTH + 'px');
		$('div.notes div.line').append(item);
	});
}

/* Language pulldown
 */
function change_language() {
	var language = $('div.language select').val();
	init_page(language);

	show_sections();
}

/* Shift buttons
 */
function shift_left(type, max) {
	var item = $('div.'  + type + ' div.line div.item').first().remove();
	$('div.' + type + ' div.line').append(item);

	var shifted = parseInt($('div.' + type).attr('shifted'));
	$('div.' + type).attr('shifted', (shifted + 1) % max);

	if (type == 'intervals') {
		highlight_sections();
	} else {
		show_sections();
	}
}

function shift_right(type, max) {
	var item = $('div.' + type + ' div.line div.item').last().remove();
	$('div.' + type + ' div.line').prepend(item);

	var shifted = parseInt($('div.' + type).attr('shifted'));
	$('div.' + type).attr('shifted', ((shifted - 1) + max) % max);

	if (type == 'intervals') {
		highlight_sections();
	} else {
		show_sections();
	}
}

/* Sharp or flat buttons
 */
function show_sharp_notes() {
	show_notes(notes_sharps);
	restore_shift();

	$('div.notes').css('margin-top', '');

	show_sections();
}

function show_flat_notes() {
	show_notes(notes_flats);
	restore_shift();

	$('div.notes').css('margin-top', '-2.5px');

	show_sections();
}

function restore_shift() {
	var shifted = parseInt($('div.notes').attr('shifted'));
	$('div.notes').attr('shifted', 0);

	while (shifted-- > 0) {
		shift_left('notes', 12);
	}
}

/* Show sections
 */
function show_sections() {
	show_scale();
	show_chords();
	show_modality_differences();
}

function show_scale() {
	$('div.scale div.line').empty();

	var name = $('div.notes div.line div.item:nth-child(1)').text();
	name += ' ' + $('div.modality div.line div.item:nth-child(1) div:nth-child(1)').text();
	$('div.scale_name div.line').text(name);

	var last = null;
	var index = 0;
	var count = 0;
	$('div.modality div.line div.item').each(function() {
		var item = $('div.notes div.line div.item').eq(index);
		var note = item.text();
		var orig = note;

		var sharp = false;
		var flat = false;

		if (last != null) {
			var diff = ((note.charCodeAt(0) - last - 1) % 7) + 0;
			if (diff == 6) {
				diff = -1;
			} else if (diff == -6) {
				diff = 1;
			}

			if (diff == 1) {
				var i = index - diff;
				note = $('div.notes div.line div.item').eq(i).text();
				note += SHARP;
				sharp = true;
			} else if (diff == -1) {
				var i = index - diff;
				note = $('div.notes div.line div.item').eq(i).text();
				note += FLAT;
				flat = true;
			}
		}
		last = note.charCodeAt(0);

		sharp = sharp ? 'yes' : 'no';
		flat = flat ? 'yes' : 'no';

		var item = '<div class="item nr' + count + '" orig="' + orig + '" sharp="' + sharp + '" flat="' + flat + '">' + note + '</div>';
		$('div.scale div.line').append(item);

		index += parseInt($(this).attr('steps'));
		count++;
	});
}

function show_chords() {
	$('div.chords div.line').empty();
	$('div.chords7 div.line').empty();

	var index = 0;
	var count = 0;
	$('div.modality div.line div.item').each(function() {
		var width = 60;
		var chord = $('div.scale div.line div.item').eq(count).text();

		var third = $('div.notes div.line div.item').eq((index + 4) % 12).text();
		var minor = (third != $('div.scale div.line div.item').eq((count + 2) % 7).attr('orig'));
		if (minor) {
			chord += '<span class="minor">m</span>';
			width += 10;
		}

		var chord7 = chord;
		var seventh = $('div.notes div.line div.item').eq((index + 11) % 12).text();
		var flat7 = (seventh != $('div.scale div.line div.item').eq((count + 6) % 7).attr('orig'));
		chord7 += '<span class="seventh">';
		if (flat7 == false) {
			chord7 += '<span class="major7">maj</span>';
			width += 30;
		}
		chord7 += '7</span>';

		var fifth = $('div.notes div.line div.item').eq((index + 7) % 12).text();
		var flat5 = (fifth != $('div.scale div.line div.item').eq((count + 4) % 7).attr('orig'));
		if (flat5) {
			chord += '<span class="flat5">' + FLAT + '5</span>';
			chord7 += '<span class="flat5">(' + FLAT + '5)</span>';
			width += 30;
		}

		minor = minor ? 'yes' : 'no';
		flat5 = flat5 ? 'yes' : 'no';
		flat7 = flat7 ? 'yes' : 'no';
		var item = '<div class="item nr' + count + '" minor="' + minor + '" flat5="' + flat5 + '"><span>' + chord + '</span></div>';
		var item7 = '<div class="item nr' + count + '" minor="' + minor + '" flat5="' + flat5 + '" flat7="' + flat7 + '"><span>' + chord7 + '</span></div>';
		$('div.chords div.line').append(item);
		$('div.chords7 div.line').append(item7);

		index += parseInt($(this).attr('steps'));
		
		$('div.nr' + count).css('width', width + 'px');
		
		count++;
	});

	highlight_sections();
}

function show_modality_differences() {
	$('div.basis div.line').empty();
	$('div.difference div.line').empty();

	var mode = parseInt($('div.modality').attr('shifted'));
	var is_major = major_modes.includes(mode);

	var basis_mode = (is_major ? major_modes[0] : minor_modes[0]);
	var language = $('div.language select').val();
	var name = '<div class="mode">' + modality[language][basis_mode] + '</div>';
	$('div.basis div.line').html(name);

	var name = '<div class="mode">' + modality[language][mode] + '</div>';
	$('div.difference div.line').html(name);

	var major_step = 0;
	var basis_step = 0;
	var mode_step = 0;

	for (i = 0; i < scale_steps.length; i++) {
		var step = scale_steps[i];

		var item = (i + 1).toString();
		if (basis_step < major_step) {
			item = FLAT + item;
		} else if (basis_step > major_step) {
			item = SHARP + item;
		}
		item = '<div class="item">' + item + '</div>';
		$('div.basis div.line').append(item);

		var item = (i + 1).toString();
		if (mode_step < major_step) {
			item = FLAT + item;
		} else if (mode_step > major_step) {
			item = SHARP + item;
		}

		var mark = (basis_step != mode_step) ? ' marked' : '';
		item = '<div class="item' + mark + '">' + item + '</div>';
		$('div.difference div.line').append(item);

		major_step += scale_steps[i];
		basis_step += scale_steps[(basis_mode + i) % scale_steps.length];
		mode_step += scale_steps[(mode + i) % scale_steps.length];
	}
}

/* Highlight sections
 */
function highlight_sections() {
	highlight_chord();
	highlight_interval();
}

function highlight_chord() {
	$('div.chords div.line div.item').removeClass('highlighted');
	$('div.chords7 div.line div.item').removeClass('highlighted');
	$('div.intervals').removeAttr('chord').removeAttr('offset');

	var index = 0;
	var count = 0;
	$('div.modality div.line div.item').each(function() {
		if ($('div.intervals div.line div.item').eq(index).text() == '1') {
			$('div.chords div.line div.item').eq(count).addClass('highlighted');
			$('div.chords7 div.line div.item').eq(count).addClass('highlighted');

			$('div.intervals').attr('chord', count);
			$('div.intervals').attr('offset', index);
		}

		index += parseInt($(this).attr('steps'));
		count++;
	});
}

function highlight_interval() {
	$('div.intervals div.line div.item').removeClass('third').removeClass('fifth').removeClass('seventh').removeClass('shaded');

	var chord = parseInt($('div.intervals').attr('chord'));
	var offset = parseInt($('div.intervals').attr('offset'));

	if (isNaN(chord) || isNaN(offset)) {
		$('div.intervals div.line div.item').addClass('shaded');
		return;
	}

	var minor = $('div.chords div.line div.item').eq(chord).attr('minor');
	var flat5 = $('div.chords div.line div.item').eq(chord).attr('flat5');
	var flat7 = $('div.chords7 div.line div.item').eq(chord).attr('flat7');

	/* Minor
	 */
	if (minor == 'yes') {
		$('div.intervals div.line div.item').eq((offset + 3) % 12).addClass('third');
	}
	m = (minor == 'no') ? 3 : 4;
	$('div.intervals div.line div.item').eq((offset + m) % 12).addClass('shaded');

	/* Flat 5
	 */
	if (flat5 == 'yes') {
		$('div.intervals div.line div.item').eq((offset + 6) % 12).addClass('fifth');
	}
	f5 = (flat5 == 'no') ? 6 : 7;
	$('div.intervals div.line div.item').eq((offset + f5) % 12).addClass('shaded');

	/* Flat 7
	 */
	if (flat7 == 'no') {
		$('div.intervals div.line div.item').eq((offset + 11) % 12).addClass('seventh');
	}
	f7 = (flat7 == 'yes') ? 11 : 10;
	$('div.intervals div.line div.item').eq((offset + f7) % 12).addClass('shaded');
}

/* Main
 */
$(document).ready(function() {
	for (const [key, value] of Object.entries(languages)) {
		var option = '<option value="' + key + '">' + value + '</option>';
		$('div.language select').append(option);
	};

	var language = Object.keys(languages)[0];
	init_page(language);

	show_sections();
});
